import System.Exit
import System.Taffybar.Hooks.PagerHints (pagerHints)

import XMonad
import XMonad.Config.Desktop
import XMonad.Hooks.DynamicLog
import XMonad.Hooks.EwmhDesktops (ewmh)
import XMonad.Hooks.ManageDocks
import XMonad.Hooks.ManageHelpers
import XMonad.Prompt
import XMonad.Prompt.ConfirmPrompt
import XMonad.Prompt.Shell
import XMonad.Util.EZConfig

--------------------------------------------------------------------------------
main = do

  -- Start xmonad using the main desktop configuration with a few
  -- simple overrides:
  xmonad $ ewmh $ pagerHints $ desktopConfig
    { terminal           = "urxvt"
    , focusFollowsMouse  = True
    , clickJustFocuses   = True
    , borderWidth        = 1
    , normalBorderColor  = "#222222"
    , focusedBorderColor = "#444444"

    , modMask            = mod4Mask -- Use the "Win" key for the mod key
    , manageHook         = myManageHook <+> manageHook desktopConfig <+> manageDocks
    , logHook            = dynamicLogString def >>= xmonadPropLog
    , startupHook        = spawn "taffybar"
    }

    `additionalKeysP` -- Add some extra key bindings:
      [ ("M-S-q", confirmPrompt myXPConfig "exit" (io exitSuccess))
      , ("M-p",   shellPrompt myXPConfig)
      , ("M-C-l", spawn "slock")
      ]


--------------------------------------------------------------------------------
-- | Customize the way 'XMonad.Prompt' looks and behaves.  It's a
-- great replacement for dzen.
myXPConfig = def
  { position          = Top
  , alwaysHighlight   = True
  , promptBorderWidth = 0
  , font              = "xft:monospace:size=9"
  }

--------------------------------------------------------------------------------
-- | Manipulate windows as they are created.  The list given to
-- @composeOne@ is processed from top to bottom.  The first matching
-- rule wins.
--
-- Use the `xprop' tool to get the info you need for these matches.
-- For className, use the second value that xprop gives you.
myManageHook = composeOne
  [ className =? "steam" -?> doFloat
  , isDialog             -?> doCenterFloat

    -- Move transient windows to their parent:
  , transience
  ]
