" ~/.vimrc
"   Author: Ross Bayer
"   Github: http://github.com/Rostepher
"
"   This is my personal vimrc file, feel free to use any portion you see fit.
"   I cannot guarantee the configurations in this file will satisfy everybody
"   and I don't reccommend just copying setting you don't understand.
"
"   Sections:
"       1.  Startup
"       2.  Terminal Settings
"       3.  General
"       4.  Vundle
"       5.  Colors
"       6.  UI
"       7.  Tabs and Spaces
"       8.  Keybindings
"       9.  Plugin Settings
"       10. Remove Trailing Whitespace
"       11. Functions
"

" Startup
    let s:colorful_term = (&term =~ "rxvt-unicode-256color")


" Termingal Settings
    if s:colorful_term
        " 256 colors
        let &t_Co=256
    endif


" General
    set nocompatible        " opt-out of vi-compatible mode

    " set encoding to utf-8
    set encoding=utf-8
    set termencoding=utf-8

    set history=1024        " keep a long history

    " die error bells, die!
    set noerrorbells
    set novisualbell

    " screw folding
    set nofoldenable

    " makes backspace act more sensibly
    set backspace=eol,start,indent

    " when pressing navigation keys, vim will move up or down a line after
    " reaching the first or last character.
    set whichwrap+=b,s,h,l,<,>,[,]

    set hlsearch            " highlight search results
    set incsearch           " incremental search, real time updating
    set ignorecase          " ignore case in searches
    set smartcase           " if a search has upper case then it is case-sensative

    set lazyredraw          " don't redraw while executing macros

    set magic               " regular expression magic

    set nobackup            " no backup files
    set noswapfile          " no swap files
    set noautowrite         " when I want to write a file, I'll say so
    set autoread            " re-read open files when they are changed outise of vim

    " make shady characters obvious
    "set listchars=trail:·,precedes:«,extends:»,eol:↲,tab:▸\
    "set list

 " NeoBundle
    filetype off            " required for NeoBundle

    " set runtime path to include NeoBundle and initialize
    set runtimepath+=~/.vim/bundle/neobundle.vim
    call neobundle#begin(expand('~/.vim/bundle'))

    " let NeoBundle manage itself, required
    NeoBundleFetch 'shougo/neobundle.vim'

    " Vimproc and Vimshell
        exec "NeoBundle 'shougo/vimproc.vim'," . string({
            \   'build' : {
            \       'cygwin' : 'make -f make_cygwin.mak',
            \       'linux' : 'make',
            \       'mac' : 'make -f make_mac.mak',
            \       'unix' : 'make -f make_unix.mak',
            \   },
            \})

    "NeoBundles
        "NeoBundle 'atweiden/vim-betterdigraphs'
        NeoBundle 'kien/ctrlp.vim'
        NeoBundle 'kien/rainbow_parentheses.vim'
        NeoBundle 'lokaltog/vim-easymotion'
        NeoBundle 'maciakl/vim-neatstatus'
        NeoBundle 'mkitt/tabline.vim'
        NeoBundle 'scrooloose/nerdtree'
        NeoBundle 'scrooloose/nerdcommenter'
        NeoBundle 'scrooloose/syntastic'
        NeoBundle 'tpope/vim-surround'
        NeoBundle 'tpope/vim-commentary'

    " Syntax Highlighting
        NeoBundle 'brainfuck-syntax'
        NeoBundle 'c-standard-functions-highlight'
        NeoBundle 'oinksoft/vim-sml'
        NeoBundle 'mips.vim'
        NeoBundle 'haskell.vim'
        NeoBundle 'cabal.vim'
        NeoBundle 'rust-lang/rust.vim'
        NeoBundle 'cespare/vim-toml'
        NeoBundle 'exu/pgsql.vim'
        NeoBundle 'adimit/prolog.vim'

    " Color Schemes
        NeoBundle 'tomasr/molokai'
        NeoBundle 'sickill/vim-monokai'

    call neobundle#end()

    " turns on the filetype detection, plugin and indent AFTER Vundles
    filetype plugin indent on

    " if there are any uninstalled bundles found on startup, this will
    " conveniently prompt you to install them.
    NeoBundleCheck


" Colors
    " moloaki settings
    let g:molokai_original = 1
    let g:rehash256 = 1

    syntax on               " syntax highlighting
    set background=dark     " dark background
    colorscheme molokai     " colorscheme


" UI
    set number              " line numbers
    set showmode            " displays the current input mode
    set ruler               " display row and col of cursor in bar
    set showcmd             " show partial commands in status line
    set showmatch           " breifly jump to a paren once it's balanced
    set matchtime=2         " show the match for 2 seconds
    set scrolloff=5         " keep the cursor at least 5 lines from the top or bottom

    set cursorline

    set mouse=a             " enables mouse in vim

    " wild menu
    set wildmenu
    set wildmode=list:longest,full

    " tab bar colors
    hi TabLine ctermfg=DarkGrey ctermbg=Black cterm=NONE
    hi TabLineFill ctermfg=DarkGrey ctermbg=Black cterm=NONE
    hi TabLineSel ctermfg=Black ctermbg=DarkGreen cterm=NONE

    " vertical split character
    set fillchars+=vert:┃       "┃ -> u2503
    hi VertSplit ctermfg=DarkGrey ctermbg=DarkGrey cterm=NONE

    " highlight 81st column of long lines
    highlight ColorColumn ctermbg=magenta
    call matchadd('ColorColumn', '\%81v', 100)


" Tabs and Spaces
    set expandtab           " expand tabs to spaces
    set smarttab
    set tabstop=4           " tab width 4
    set shiftwidth=4        " expanded tab to 4 spaces
    set autoindent
    set smartindent

    " force assembly files to use a tabwidth of 8, no expand and mips asm
    autocmd FileType asm setlocal noexpandtab
                                \ shiftwidth=8
                                \ tabstop=8
                                \ syntax=mips

    " force fstab to use tabs
    autocmd FileType fstab setlocal noexpandtab
                                  \ shiftwidth=8
                                  \ tabstop=8

    " force Makefiles to use tabs
    autocmd FileType make setlocal noexpandtab

    " force SML files to use sml syntax highlighting
    " autocmd FileType sml setlocal shiftwidth=3
    "                             \ tabstop=3
    "                             \ expandtab

    " force LangF files to use sml syntax highlighting
    autocmd BufNewFile,BufRead *.lgf,*.langf setlocal filetype=langf
    autocmd FileType langf setlocal syntax=sml

    " force *.pl files to use prolog syntax
    autocmd BufNewFile,BufRead *.pl setlocal filetype=prolog
    autocmd FileType prolog setlocal syntax=prolog

    " force HTML and CSS files to use a tabwidth of 2
    autocmd FileType html,css setlocal shiftwidth=2
                                     \ tabstop=2

    " force JavaScript files to use a tabwidth of 2
    autocmd FileType javascript setlocal shiftwidth=2
                                       \ tabstop=2

    " force SQL files to use psql syntax highlighting
    autocmd FileType sql setlocal syntax=pgsql

    " force Ruby files to use tabwidth of 2
    autocmd FileType ruby setlocal shiftwidth=2
                                 \ tabstop=2

" Keybindings
    nnoremap ; :
    let mapleader = ","


" Plugin Settings
    let b:did_indent = 1 " turn off sml-vim indenting crap

    autocmd FileType cmake setlocal commentstring=#%s
    autocmd FileType sml setlocal commentstring=(*%s*)

    " syntastic setup
    set statusline+=%#warningmsg#
    set statusline+=%{SyntasticStatuslineFlag()}
    set statusline+=%*

    let g:syntastic_always_populate_loc_list = 1
    let g:syntastic_auto_loc_list = 1
    let g:syntastic_check_on_open = 0
    let g:syntastic_check_on_wq = 0

    let g:syntastic_mode_map = {
    \   'mode': 'passive',
    \   'active_filetypes': [],
    \   'passive_filetypes': []
    \ }

    " ctl-w E to run syntastic
    nnoremap <C-w>E :SyntasticCheck<CR> :SyntasticToggleMode<CR>


" Remove Trailing Whitespace
    " highlight trailing whitespace
    match ErrorMsg '\s\+$'

    " removes trailing whitespace
    function! RemoveTrailingWhitespace()
        %s/\s\+$//e
    endfunction

    " map func to rts
    nnoremap <silent> <Leader>rts :call RemoveTrailingWhitespace()<CR>

    autocmd FileWritePre    * :call RemoveTrailingWhitespace()
    autocmd FileAppendPre   * :call RemoveTrailingWhitespace()
    autocmd FilterWritePre  * :call RemoveTrailingWhitespace()
    autocmd BufWritePre     * :call RemoveTrailingWhitespace()


" Functions
