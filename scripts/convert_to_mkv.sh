#!/bin/bash

for file in *.mp4 
do
    filename=$(basename $file)
    ext=${filename##*.}
    filename=${filename%.*}
    echo "${file} -> mkv/${filename}.mkv"
    ffmpeg -i ${file} -codec copy ${filename}.mkv
done
