#!/bin/bash

for file in *.cbr
do
    # find the basename of the file without the extension
    name=$(basename $file | sed 's/\(.*\)\.cbr/\1/')
    echo "$file -> $name.cbz"

    # create temp directory
    mkdir -p temp

    # extract the rar file
    rar e $file ./temp > /dev/null 2>&1 &&
        echo -e "\t$file extracted" || "\trar extraction failed"

    # zip up the new comic
    zip $name.cbz ./temp/* > /dev/null 2>&1 &&
        echo -e "\t$name.cbz created" || "\tzip failed"

    # remove temp dir
    rm -r temp
done
