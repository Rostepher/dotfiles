#!/bin/bash

# fix the screen orientationa and configuration
xrandr --output DVI-D-0 --primary --preferred
xrandr --output DVI-I-0 --preferred --right-of DVI-D-0

# set the background
feh --bg-fill ~/pictures/wallpapers/oh_great_a_background.jpg
