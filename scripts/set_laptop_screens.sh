#!/bin/bash

# fix the screen orientationa and configuration
xrandr --output VGA1 --primary --preferred
xrandr --output LVDS1 --preferred --right-of VGA1

# set the background
feh --bg-fill ~/pictures/wallpapers/oh_great_background.jpg
