# dotfiles

Personal configuration files for my customized environment. Feel free to read,
use and improve to your heart's content.
