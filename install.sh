#!/bin/bash

# This script creates symlinks from a user's home directory to files and
# directories in ~/dotfiles. Therefore this script assumes that the dotfiles
# directory is located in the user's home directory.

# unofficial bash strict mode
set -eu
set -o pipefail
IFS=$'\n\t'

# constants
readonly ARGS="$@"
readonly DOTFILES=~/dotfiles
readonly OLD_DIR=~/old_dotfiles

# files and directories to symlink
FILES=(
    "vim"
    "vimrc"
    "xorg/xinitrc"
    "xorg/xmodmap"
    "xmonad"
    "xorg/Xresources"
    "zprofile"
    "zshrc"
)

# installs oh-my-zsh if not already installed
install_oh_my_zsh() {
    # check if oh-my-zsh exists
    if [[ ! -d ~/.oh-my-zsh ]]; then
        echo "installing oh-my-zsh"
        git clone git://github.com/robbyrussell/oh-my-zsh.git ~/.oh-my-zsh
    else
        echo "oh-my-zsh is already installed"
    fi
}

# installs neobundle for vim if not already installed
install_neobundle() {
    # check if neobundle exists
    if [[ ! -d $DOTFILES/vim/bundle/neobundle.vim ]]; then
        echo "installing neobundle.vim"
        mkdir -p $DOTFILES/vim/bundle
        git clone https://github.com/Shougo/neobundle.vim $DOTFILES/vim/bundle/neobundle.vim
    else
        echo "neobundle.vim is already installed"
    fi
}

# configures vim symlinks and checks for neovim
configure_neovim() {
    if type nvim > /dev/null 2>&1; then
        mkdir -p ${XDG_CONFIG_HOME:=$HOME/.config}
        ln -s $DOTFILES/vim $XDG_CONFIG_HOME/nvim
        ln -s $DOTFILES/vimrc $XDG_CONFIG_HOME/nvim/init.vim
    fi
}

# sets /bin/zsh to the current shell, if it exists and is not already the users
# current shell, then it installs oh-my-zsh
configure_zsh() {
    # test to see if zsh is installed
    if [[ -f /etc/shells && $(cat /etc/shells | grep "/zsh") ]]; then
        echo "configuring zsh"
        local zsh=$(which zsh)

        # test if zsh is the current shell, if not change it
        if [[ ! $SHELL == $zsh ]]; then
            echo "changing default shell to $zsh"
            chsh -s $zsh

            if [[ ! $? == 0 ]]; then
                echo "could not change shell to $zsh"
            fi
        fi

        # install oh-my-zsh if zsh is the current shell
        if [[ $(echo $SHELL) == $zsh ]]; then
            FILES+=("zshrc")
            install_oh_my_zsh
        fi
    else
        echo "please install zsh and re-run this script!"
        exit
    fi
}

# symlinks all files in FILES to the users home directory as hidden files
symlink_files() {
    for file in ${FILES[@]}
    do
        # save the basename of the file for nested files
        local base=$(basename $file)

        # move any existing dotfiles in homedir to OLD_DIR
        if [[ -e ~/.$base || -L ~/.$base ]]; then
            echo "moving ~/.$base to $OLD_DIR/$base"
            mkdir -p $OLD_DIR
            mv ~/.$base -T $OLD_DIR/.$base
        fi

        # create symlink for file in homedir
        echo "creating symlink ~/.$base"
        ln -s $DOTFILES/$file -T ~/.$base

        if [[ ! $? == 0 ]]; then
            echo "failed to create symlink ~/.$base"
        fi
    done
    unset $file
}

# main function
main() {
    # remove OLD_DIR if it exists
    if [[ -d ~/$OLD_DIR ]]; then
        rm -r ~/$OLD_DIR
    fi

    # run installation steps
    configure_neovim
    configure_zsh
    install_neobundle
    symlink_files
}

main
