#!/bin/sh

# initial path
PATH="/bin:/usr/bin:/usr/sbin:/usr/local/bin:/usr/local/sbin:/opt"

# rvm setup
RVM="$MY_RUBY_HOME/bin:$(echo $GEM_PATH | sed 's/:/\/bin:/g')/bin"

# language specific path settings
HASKELL="$HOME/.cabal/bin"
JAVA="/opt/java/bin"
PERL="/usr/bin/core_perl:/usr/bin/vendor_perl"
RUBY="$HOME/.rvm/bin:$RVM"
RUST="$HOME/.cargo/bin"
SML="/usr/lib/smlnj/bin"

# export the completed path
export PATH="$HASKELL:$SML:$PERL:$RUBY:$RUST:$JAVA:$PATH:$HOME/.local/bin"

export BROWSER="chromium"

# set preferred editor
export EDITOR="nvim"
export GIT_EDITOR=$EDITOR
