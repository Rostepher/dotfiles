#!/bin/bash

# compiles given file in current directory
compile() {
    if [[ $# -le 0 ]]; then
        echo "Usage: compile [source]"
        return
    fi

    local CC=clang
    local PKGS=ncurses
    local SOURCE=${1%%.c}

    $CC -std=c99 -Wall -ggdb $(pkg-config --cflags $PKGS)\
        $SOURCE.c $(pkg-config --libs $PKGS) -o $SOURCE
}

# non-recursively converts all filenames to lowercase and spaces to underscores
tolower() {
    if [[ $# -le 0 ]]; then
        echo "Usage: tolower [file pattern]"
        return
    fi

    for f in $*
    do
        # check if $f is a file
        if [[ -f $f ]]; then
            local name=$(echo $f | tr '[A-Z]' '[a-z]' | tr ' ' '_')
            if [[ $name != $f ]]; then
                mv -i $f $name
            fi
        fi
    done
}

# Removes orphaned packages
orphans() {
    if [[ ! -n $(pacman -Qdt) ]]; then
        echo "no orphaned packages"
    else
        sudo pacman -Rns $(pacman -Qqdt)
    fi
}
