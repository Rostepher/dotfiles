#!/bin/bash

# aliases
alias c="clear"
alias l="ls -l"
alias ll="ls -al"
alias rename="perl-rename"
alias tree="tree -C"
alias vi="vim"
alias emacs="emacs -nw"

# neovim
if type nvim > /dev/null 2>&1; then
  alias vim="nvim"
fi

# chromium ui scaling workaround
alias chromium="chromium --force-device-scale-factor=1"

# interpreters
alias irb="irb --simple-prompt"
#alias ghci="ghci"
alias python="python3"
alias pip="pip3"
alias ipython="ipython3"
