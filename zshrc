# Path to your oh-my-zsh configuration.
ZSH=~/.oh-my-zsh

# Set oh-my-zsh theme
ZSH_THEME="gentoo"

source $ZSH/oh-my-zsh.sh

# source custom zsh settings
if [[ -d $HOME/dotfiles/zsh ]]; then
    for config_file in $HOME/dotfiles/zsh/*.sh; do
        source $config_file
    done
fi

# Load RVM into a shell session *as a function*
[[ -s "$HOME/.rvm/scripts/rvm" ]] && source "$HOME/.rvm/scripts/rvm"

export PATH="$PATH:$HOME/.rvm/bin" # Add RVM to PATH for scripting

# OPAM configuration
. /home/rostepher/.opam/opam-init/init.zsh > /dev/null 2> /dev/null || true
