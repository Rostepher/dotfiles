import System.Information.Memory
import System.Information.CPU

import System.Taffybar
import System.Taffybar.Systray
import System.Taffybar.TaffyPager
import System.Taffybar.SimpleClock
import System.Taffybar.Widgets.PollingBar
import System.Taffybar.Widgets.PollingGraph

memCallback = do
  memInfo <- parseMeminfo
  return [memoryUsedRatio memInfo]

cpuCallback = do
  (_, systemLoad, totalLoad) <- cpuLoad
  return [totalLoad, systemLoad]

main = do
  let memCfg = defaultGraphConfig { graphDataColors = [(1, 0, 0, 1)]
                                  , graphLabel = Just "mem"
                                  }
      cpuCfg = defaultGraphConfig { graphDataColors = [ (0, 1, 0, 1)
                                                      , (1, 0, 1, 0.5)
                                                      ]
                                  , graphLabel = Just "cpu"
                                  }
  let clock = textClockNew Nothing "<span fgcolor='gray'>%a %b %_d %H:%M</span>" 1
      pager = taffyPagerNew defaultPagerConfig
      mem = pollingGraphNew memCfg 1 memCallback
      cpu = pollingGraphNew cpuCfg 0.5 cpuCallback
      tray = systrayNew

  defaultTaffybar defaultTaffybarConfig
    { barHeight = 20
    , startWidgets = [pager]
    , endWidgets = [tray, clock, mem, cpu]
    }
